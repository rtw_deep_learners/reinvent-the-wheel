# -*- coding: utf-8 -*-
"""
Created on Sat Mar 30 15:52:19 2019

@author: dishant
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


df = pd.read_csv("X_train.csv")
Y=pd.read_csv("Y_train.csv")


df=df.drop('SELLING_PRICE', axis=1)
df=df.drop('idx', axis=1)
df=df.drop('Monthly_Top_1_Customer_Zip', axis=1)
df=df.drop('Monthly_Top_2_Customer_Zip', axis=1)
df=df.drop('Monthly_Top_3_Customer_Zip', axis=1)
df=df.drop('Monthly_Top_4_Customer_Zip', axis=1)
df=df.drop('Monthly_Top_5_Customer_Zip', axis=1)
df=df.drop('Monthly_Top_6_Customer_Zip', axis=1)
df=df.drop('Monthly_Top_7_Customer_Zip', axis=1)
df=df.drop('Monthly_Top_8_Customer_Zip', axis=1)
df=df.drop('Monthly_Top_9_Customer_Zip', axis=1)
df=df.drop('Monthly_Top_10_Customer_Zip', axis=1)
df=df.drop('DC_ZIPCODE', axis=1)
df=df.drop('Invoice_Year', axis=1)
df=df.drop('Unnamed: 0',axis=1)
#df=df.drop('RIM_DIAMETER_SIZE_CODE',axis=1)
#df=df.drop('WIDTH',axis=1)
#df=df.drop('HEIGHT',axis=1)
#df=df.drop('Invoice_Week',axis=1)
#df=df.drop('Invoice_Month',axis=1)

df['SELLING_PRICE']=df['SELLING_PRICE'].replace('=',' ')
df['SELLING_PRICE'].fillna(df['SELLING_PRICE'].mean(), inplace=True)
df['Monthly_Top_1_Customer_Total_Sales'].fillna(df['Monthly_Top_1_Customer_Total_Sales'].mean(), inplace=True)
df['Monthly_Top_2_Customer_Total_Sales'].fillna(df['Monthly_Top_2_Customer_Total_Sales'].mean(), inplace=True)
df['Monthly_Top_3_Customer_Total_Sales'].fillna(df['Monthly_Top_3_Customer_Total_Sales'].mean(), inplace=True)
df['Monthly_Top_4_Customer_Total_Sales'].fillna(df['Monthly_Top_4_Customer_Total_Sales'].mean(), inplace=True)
df['Monthly_Top_5_Customer_Total_Sales'].fillna(df['Monthly_Top_5_Customer_Total_Sales'].mean(), inplace=True)
df['Monthly_Top_6_Customer_Total_Sales'].fillna(df['Monthly_Top_6_Customer_Total_Sales'].mean(), inplace=True)
df['Monthly_Top_7_Customer_Total_Sales'].fillna(df['Monthly_Top_7_Customer_Total_Sales'].mean(), inplace=True)
df['Monthly_Top_8_Customer_Total_Sales'].fillna(df['Monthly_Top_8_Customer_Total_Sales'].mean(), inplace=True)
df['Monthly_Top_9_Customer_Total_Sales'].fillna(df['Monthly_Top_9_Customer_Total_Sales'].mean(), inplace=True)
df['Monthly_Top_10_Customer_Total_Sales'].fillna(df['Monthly_Top_10_Customer_Total_Sales'].mean(), inplace=True)


dummies_df_tier=pd.get_dummies(df["TIER"], columns=['Tier1','Tier 2', 'Tier 3', 'Tier4'] )
dummies_df_Cat=pd.get_dummies(df["CATEGORY"], columns=['Light Truck','Performance Sport','Passenger Car / Mini-Van','SUV CUV','Winter','Luxury Touring','HT SUV CUV ','AT Mud Utility','Performance Sport Car ', 'Standard Passenger '] )
dummies_Speed=pd.get_dummies(df["SPEED_RATING_CODE"], columns=['T', 'S', 'H ','W','V','R','Q','Y','(Y)','L','N','M','P','Z'  ])


df=df.join(dummies_df_tier)
df=df.join(dummies_df_Cat)
df=df.join(dummies_Speed)

df=df.drop('CATEGORY', axis=1)
df=df.drop('TIER', axis=1)
df=df.drop('SPEED_RATING_CODE', axis=1)


from sklearn.decomposition import PCA
pca = PCA(n_components=9)
df=pca.fit_transform(df)
variance=pca.explained_variance_ratio_

from sklearn.preprocessing import StandardScaler
df = StandardScaler().fit_transform(df)


from sklearn.cross_validation import train_test_split
df_train, df_test, y_train, y_test=train_test_split(df, Y["UNITS"], test_size=0.2, random_state=0)


import statsmodels.api as sm
model=sm.OLS(y_train, df_train).fit()
model.summary()

y_pred=model.predict(df_test)


#from sklearn.ensemble import RandomForestRegressor
#reg = RandomForestRegressor(n_estimators=100)
#reg.fit(df_train, y_train)
#y_pred=reg.predict(df_test)

#from sklearn.ensemble import GradientBoostingRegressor
#reg = GradientBoostingRegressor(n_estimators=50)
#reg.fit(df_train, y_train)
#y_pred=reg.predict(df_test)


#from xgboost import XGBRegressor 
#XGB = XGBRegressor(n_estimators=100,max_depth=10)

#XGB = XGBRegressor(n_estimators=5, learning_rate=0.02, gamma=0, max_depth=10)
                           #colsample_bytree=1, max_depth=10)
#XGB.fit(df_train,y_train)
#y_pred = XGB.predict(df_test)


#from sklearn.model_selection import GridSearchCV
#parameters = [{'n_estimators': [10], 
#              'learning_rate': [0.01, 0.02, 0.03, 0.04, 0.05],
#             'gamma': [0.1, 0.2, 0.3, 0.4, 0.5],
#             'max_depth': [10,25]}]
#grid_search = GridSearchCV(estimator = XGB,
#                           param_grid = parameters,
#                           scoring = 'neg_mean_squared_error',
#                           cv = 10,
#                           n_jobs = -1)
#grid_search = grid_search.fit(df_train, y_train)
#best_accuracy = grid_search.best_score_
#best_parameters = grid_search.best_params_

#df['SELLING_PRICE'] = df['SELLING_PRICE'].replace('-inf', '')
#df['SELLING_PRICE'] = df['SELLING_PRICE'].str.re  place('?', '').astype('float')
#df['SELLING_PRICE'].fillna(df['SELLING_PRICE'].mean(), inplace=True)

#from keras.layers import Dense, Activation
#from keras.layers import Activation
#from keras.models import Sequential

#model = Sequential()
#model.add(Dense(12, activation = 'linear', input_dim = 44))
#model.add(Dense(units = 8, activation = 'linear'))
#model.add(Dense(1,))
#model.compile(optimizer='adam', loss='mean_squared_error')

#model.fit(df_train, y_train, batch_size = 10, epochs = 3)

#y_pred = model.predict(df_test)

from sklearn.metrics import mean_squared_error
from math import sqrt

rms = sqrt(mean_squared_error(y_test, y_pred))
print(rms)


import pandas as pd

dfT = pd.read_csv("X_test.csv")

dfT=dfT.drop('SELLING_PRICE', axis=1)
dfT=dfT.drop('idx', axis=1)
dfT=dfT.drop('Monthly_Top_1_Customer_Zip', axis=1)
dfT=dfT.drop('Monthly_Top_2_Customer_Zip', axis=1)
dfT=dfT.drop('Monthly_Top_3_Customer_Zip', axis=1)
dfT=dfT.drop('Monthly_Top_4_Customer_Zip', axis=1)
dfT=dfT.drop('Monthly_Top_5_Customer_Zip', axis=1)
dfT=dfT.drop('Monthly_Top_6_Customer_Zip', axis=1)
dfT=dfT.drop('Monthly_Top_7_Customer_Zip', axis=1)
dfT=dfT.drop('Monthly_Top_8_Customer_Zip', axis=1)
dfT=dfT.drop('Monthly_Top_9_Customer_Zip', axis=1)
dfT=dfT.drop('Monthly_Top_10_Customer_Zip', axis=1)
dfT=dfT.drop('DC_ZIPCODE', axis=1)
dfT=dfT.drop('Invoice_Year', axis=1)
dfT=dfT.drop('Unnamed: 0',axis=1)
#dfT=dfT.drop('RIM_DIAMETER_SIZE_CODE',axis=1)
#dfT=dfT.drop('WIDTH',axis=1)
#dfT=dfT.drop('HEIGHT',axis=1)
#dfT=dfT.drop('Invoice_Week',axis=1)
#dfT=dfT.drop('Invoice_Month',axis=1)




dfT['Monthly_Top_1_Customer_Total_Sales'].fillna(dfT['Monthly_Top_1_Customer_Total_Sales'].mean(), inplace=True)
dfT['Monthly_Top_2_Customer_Total_Sales'].fillna(dfT['Monthly_Top_2_Customer_Total_Sales'].mean(), inplace=True)
dfT['Monthly_Top_3_Customer_Total_Sales'].fillna(dfT['Monthly_Top_3_Customer_Total_Sales'].mean(), inplace=True)
dfT['Monthly_Top_4_Customer_Total_Sales'].fillna(dfT['Monthly_Top_4_Customer_Total_Sales'].mean(), inplace=True)
dfT['Monthly_Top_5_Customer_Total_Sales'].fillna(dfT['Monthly_Top_5_Customer_Total_Sales'].mean(), inplace=True)
dfT['Monthly_Top_6_Customer_Total_Sales'].fillna(dfT['Monthly_Top_6_Customer_Total_Sales'].mean(), inplace=True)
dfT['Monthly_Top_7_Customer_Total_Sales'].fillna(dfT['Monthly_Top_7_Customer_Total_Sales'].mean(), inplace=True)
dfT['Monthly_Top_8_Customer_Total_Sales'].fillna(dfT['Monthly_Top_8_Customer_Total_Sales'].mean(), inplace=True)
dfT['Monthly_Top_9_Customer_Total_Sales'].fillna(dfT['Monthly_Top_9_Customer_Total_Sales'].mean(), inplace=True)
dfT['Monthly_Top_10_Customer_Total_Sales'].fillna(dfT['Monthly_Top_10_Customer_Total_Sales'].mean(), inplace=True)


dummies_dfT_tier=pd.get_dummies(dfT["TIER"], columns=['Tier1','Tier 2', 'Tier 3', 'Tier4'] )
dummies_dfT_Cat=pd.get_dummies(dfT["CATEGORY"], columns=['Light Truck','Performance Sport','Passenger Car / Mini-Van','SUV CUV','Winter','Luxury Touring','HT SUV CUV ','AT Mud Utility','Performance Sport Car ', 'Standard Passenger '] )
dummies_Speed=pd.get_dummies(dfT["SPEED_RATING_CODE"], columns=['T', 'S', 'H ','W','V','R','Q','Y','(Y)','L','N','M','P','Z'  ])


dfT=dfT.join(dummies_dfT_tier)
dfT=dfT.join(dummies_dfT_Cat)
dfT=dfT.join(dummies_Speed)

dfT=dfT.drop('CATEGORY', axis=1)
dfT=dfT.drop('TIER', axis=1)
dfT=dfT.drop('SPEED_RATING_CODE', axis=1)

from sklearn.preprocessing import StandardScaler
dfT = StandardScaler().fit_transform(dfT)

model=sm.OLS(Y["UNITS"], df).fit()
Y_pred2=model.predict(dfT)


#from sklearn.decomposition import PCA
#pca = PCA(n_components=2)
#dfT=pca.fit_transform(dfT)
#variance=pca.explained_variance_ratio_

#from xgboost import XGBRegressor 
#XGB = XGBRegressor(n_estimators=100,max_depth=10)
#XGB.fit(df,Y["UNITS"])
#Y_pred2 = XGB.predict(dfT)

#Y_pred2 = model.predict(dfT)



#reg = RandomForestRegressor(n_estimators=25)
#reg.fit(df, Y["UNITS"])
#Y_pred2=reg.predict(dfT)


import pandas as pd 
import numpy as np 
from sklearn.metrics import mean_squared_error
import os


def submit_score(predictions, team_key):
    """
    Submit your predictions for scoring

    Args:
        predictions (DataFrame): Pandas DataFrame containing the following required
            column:
                1. idx (int) - The unique identifier for each observation
                2. predictions (float) - Your predicted value
        team_key (str): Your team's unique identifier

    Returns:
        Response: Flask Response object. See the Response.text field to get the score
            from your latest submission. Your best score will be reflected on the
            leaderboard
    """

    import requests
    import json
    import numpy
    def default(o):
        if isinstance(o, numpy.int64):
            return int(o)
        raise TypeError

    API_ENDPOINT = "http://coe-hackathon-dot-atd-fn-anacoe-dev.appspot.com/submitscore"
    payload = {
        "team_key": team_key,
        "data": predictions.loc[:, ["idx", "predictions"]].to_dict(orient="records")
    }
    resp = requests.post(
        API_ENDPOINT,
        data=json.dumps(payload, default=default),
        headers={'Content-Type': 'application/json'}
    )
    
    if resp.status_code == 404:
        print(resp.json()['error'])
        return None
    
    elif resp.status_code != 200:
        raise ValueError('There was an error processing your request: '
                         '\n{}'.format(resp.text))
        return None
    else:
        score = resp.json()['score']
        print('Submission successful! Your score was \n{}'.format(score))
        return score
    
testdatasize = 456460
    
teamkey = '$pbkdf2-sha512$25000$C4FQCuGc03rvvbcWIkSIcQ$NdtcJPp1J7w4Yq5sLYloqpoYSSNhha.K4byxofUm4SaUOcF0uMFnQreKft0V1ws286WpO0y1sHatBkO..aEWTQ'


df1 = pd.read_csv("X_test.csv")


testX = pd.DataFrame()
#testX = df.fillna(0)


testX['idx'] =df1["X"]
testX["predictions"] = Y_pred2
#testX["predictions"] = df1["predict"]

import time
s = time.time()
a = submit_score(testX[['idx','predictions']],teamkey)
print('submission time elapsed: '+str(time.time() - s))

